<?php
/*
Template Name: my account page
Template Post Type: post, page, event
*/

 
get_header();  

global $current_user; wp_get_current_user(); 
$user_id = get_current_user_id();

$all_meta_for_user = get_user_meta( $user_id );
$userdetails = [];
foreach($all_meta_for_user as $key => $userdetail) {

	$userdetails[$key] = $userdetail[0];
	//print_r($userdetails);
}

$user_info = get_userdata($user_id);

$manpower_id = $user_info->ManpowerReqDetailId;
$mailadres = $user_info->user_email;
?>

<?php

if($manpower_id) {

	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://sparta-api.azurewebsites.net/api/manpowers/lodgeReservation/' . $manpower_id,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
	    'Authorization: ' . getToken()
	  ),
	));

	$pcresponse = curl_exec($curl);

	curl_close($curl);
	$manpower_dets = json_decode($pcresponse);
	// echo '<pre>';
	// print_r($manpower_dets);
	// echo '</pre>';

}

?>


 <style type="text/css">
 </style>

		<div class="container custom-page-container">
			<div class="row">
				<div class="left-section col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
					<ul class="custom-page-account-section">
						<li><a href="/account-page/"> <u>My Account</u></a></li>
						<?php
							$user = wp_get_current_user();
							if ( in_array( 'um_employee-day-1', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list"><a href="/wp-content/uploads/Ignite2021_WelcomePacket_Aug2_20210724a.pdf" target="_blank" download> <u>Welcome Packet</u></a></li>
								<?php
							}
							if ( in_array( 'um_notemployee', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list"><a href="/wp-content/uploads/Ignite2021_WelcomePacket_Aug2_20210724a.pdf" target="_blank" download> <u>Welcome Packet</u></a></li>
								<?php
							}
							if ( in_array( 'um_employee-day-2', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list"><a href="/wp-content/uploads/Ignite2021_WelcomePacket_Aug2-3_20210724a.pdf" target="_blank" download> <u>Welcome Packet</u></a></li>
								<?php
							}
						?>
						<li class="rsvp-list"><a href="/rsvp-status/">RSVP status</a></li>
						
					</ul>
				</div>
				<div class="right-section col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<div class="cust-page-title">My account</div>
					<div class="cust-user-name">
						<h1>Welcome, <?php echo $current_user->display_name; ?>.</h1>
						<p>You can now save your spot for <strong><i>Ignite 2021.</i></strong> Please let us know if you are going to join us.</p>
						<p style="color: #f00; font-style: italic;">All RSVPs are final after submission and any changes will not be received.</p>
					</div>
					<div class="name-field cust-fields"><strong>Name:</strong> <span class="field-text"><?php echo $current_user->display_name; ?></span></div>
					<div class="comapny-field cust-fields"><strong>Company:</strong> <span class="field-text"><?php echo $userdetails['Company']; ?></span></div>
					<div class="mail-field cust-fields"><strong>Email address:</strong> <span class="field-text"><?php echo $mailadres; ?></span></div>
					<div class="rsvp-field cust-fields"><strong>RSVP:</strong> <span class="field-text"><?php if ($userdetails['RSVP'] == 'true') echo 'Yes'; else echo 'Not Specified'; ?></span></div>
					
					<?php

						if($manpower_id && $manpower_dets){
							if($manpower_dets->LodgeName)
								echo "<div class='hotel-field cust-fields'><strong>Hotel: </strong> <span class='field-text'> {$manpower_dets->LodgeName}</span></div>";

							if($manpower_dets->ReservationNumber)
								echo "<div class='ReservationNumber-field cust-fields'><strong>Reservation #: </strong> <span class='field-text'> {$manpower_dets->ReservationNumber}</span></div>";

							if($manpower_dets->CheckInDate) {
								$manpower_dets->CheckInDate = substr($manpower_dets->CheckInDate, 0, strpos($manpower_dets->CheckInDate, "T"));
								echo "<div class='CheckInDate-field cust-fields'><strong>Check in Date: </strong> <span class='field-text'> {$manpower_dets->CheckInDate}</span></div>";							
							}
							if($manpower_dets->CheckOutDate) {
								$manpower_dets->CheckOutDate = substr($manpower_dets->CheckOutDate, 0, strpos($manpower_dets->CheckOutDate, "T"));
								echo "<div class='CheckOutDate-field cust-fields'><strong>Check out Date: </strong> <span class='field-text'> {$manpower_dets->CheckOutDate}</span></div>";
							}
						}

					?>


				</div>
				
				
			</div>
		</div>




<?php  get_footer(); ?>