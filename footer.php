<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" class="site-footer container-fluid py-5 custom-footer">
		<div class="container footer-main" >
			<div class="row">
				
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 cust-footer-left">
				<img src="/wp-content/themes/bigsplash/assets/images/UPS_Black_Logo.svg" class="svg-black" style="width:70%">
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-9 col-12 footer-text cust-footer-middle">
				As the only specialists in rotating, fixed, reciprocating, and electrical equipment in the United States, Universal Plant Services is your single source for service expertise, innovative solutions and a superior customer-focused experience.</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 cust-footer-right">
									<?php wp_nav_menu('footer'); ?>
						
			</div>
				</div>
		</div>
		<div class="container footer-bottom" >
			 <div class="row">
			<div class="col-xl-12 footer-bottom-text">
				&copy; 2021 Universal Plant Services All rights reserved. Terms and Conditions

			</div>
			</div>
		</div>

	</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>
