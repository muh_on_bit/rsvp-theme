<!DOCTYPE html>
<html lang="en-US">
<head>
	 
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	
 
 

<?php
	wp_head();
	 ?>

</head>

<?php
	$browser = new Wolfcast\BrowserDetection();
	if($browser->getPlatform() == Wolfcast\BrowserDetection::PLATFORM_IOS):
		$ios = 'ios';
	else:
		$ios = '';
	endif;
?>

<body <?php body_class($ios, $formpage); ?>>
<div id="page" class="site">
	<?php do_action( 'before' ); ?>

<!-- updated boot strap menu -->
	<nav class="navbar-inverse cust-nav">
		<div class="container-fluid cust-menu">
			<div class="row cust-row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-12 left-menu">
					<?php 
						$url = home_url();
					?>
					<a href="<?php echo $url; ?>"><img style="width:200px;" src="/wp-content/themes/bigsplash/assets/images/Ignite2021_Logo.svg"></a>
				</div>
				
				<div class="col-lg-8 col-md-8 col-sm-12 col-12 right-menu">
					<div class="cust-menu">
						<ul class="" style="display:flex">
							<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
							<div class="menu-menu-1-container">
								<ul id="menu-menu-1" class="menu">
									<?php global $current_user; wp_get_current_user(); ?>
									<?php if ( is_user_logged_in() ) { ?>
									<li class="nav-item dropdown">
										<a class="nav-link py-0 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										  Hi, <?php echo $current_user->display_name; ?>
										</a>
										<div class="dropdown-menu bg-dark w-100" aria-labelledby="navbarDropdown">
										  <a class="dropdown-item font-16" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
										</div>
									</li>
									<?php
																	 }
									else { ?>
									<li class="menu-item menu-item-type-custom menu-item-object-custom text-white"><a href="/login">Login</a></li>
									<?php } ?>
								</ul>
							</div>
						</ul>
					</div>
				</div> 
			</div>
		</div>
	</nav>
	
<!--end- updated bootstrap menu 	 -->
	
	
	
	
	
	
	
	<div id="main">
