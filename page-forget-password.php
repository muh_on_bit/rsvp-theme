<?php
/*
Template Name: Forget password template
Template Post Type: post, page, event
*/
// Page code here...


 get_header("two");

?>
 
	<div class="row full-width-section frgt-pwd-pg mx-0">
		<div class="col-md-10 mx-auto my-auto text-center">
<!-- 			<p class="sub-heading white-clr">Forget your Password?</p> -->
			<h1 class="heading bold text-uppercase white-clr">CREATE A PASSWORD</h1>
			<?php 
				echo do_shortcode( '[ultimatemember_password]' );
			?>
			 
		</div>
		
	</div>




<?php  get_footer("two"); ?>