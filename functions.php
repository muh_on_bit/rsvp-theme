<?php
//require( "classes/KLogger.php");
require_once("inc/general.php");
require_once("inc/admin.php");
require_once("inc/post_type.php");
require_once("inc/scripts.php");
require_once("inc/mail.php");
require_once("inc/BrowserDetection.php");
//require( "inc/ajax.php");


// Make some Fields Disabled on rendering of form ID 1
add_filter( 'gform_pre_render_1', 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(".disabled input").attr("readonly","readonly");
        });
    </script>
    <?php
    return $form;
}




// Getting The Authorization Token from API
function getToken() {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://universalplant.auth0.com/oauth/token/',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS => 'grant_type=password&username=events-api%40universalplant.com&password=BigSplashUPS!&client_id=PQAW2hCZheDemAHXQslBlt1pw8MvQ7Xj&audience=security.api.universalplant.com',
	  CURLOPT_HTTPHEADER => array(
	    'Content-Type: application/x-www-form-urlencoded'
	  ),
	));

	$response = curl_exec($curl);
	
	curl_close($curl);

	$result = json_decode($response, true);


	$auth_token = $result['token_type'].' '.$result['access_token'];
	return $auth_token;
 }


add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) {
	$token = getToken();

	$curl = curl_init();
    $host_url = "https://sparta-api.azurewebsites.net" ;
	curl_setopt_array($curl, array(
		CURLOPT_URL => $host_url."/api/event/LDRS22.96",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => 	array(
			"Authorization: {$token}"
		)
		
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	$results = json_decode($response);


	$custom_user_fields = $results[0];
	$custom_user_fields->HotelAccomodations = 'Not Specified';
	$custom_user_fields->TShirtSize = 'Not Specified';
	$custom_user_fields->isAttending = 'Not Specified';
	$custom_user_fields->LastEmailSent = 'Not Specified';
	$custom_user_fields->Class_1 = 'Not Specified';
	$custom_user_fields->Class_2 = 'Not Specified';
	$custom_user_fields->Class_3 = 'Not Specified';
	$custom_user_fields->Class_4 = 'Not Specified';
	$custom_user_fields->Class_5 = 'Not Specified';
	$custom_user_fields->Class_6 = 'Not Specified';
	$custom_user_fields->PreferredName = 'Not Specified';
	$custom_user_fields->CellNumber = 'Not Specified';
	//$custom_user_fields->DefaultCellPhone = 'Not Specified';


	?>
	<input type="hidden" name="custom_user_fields" value='<?php echo implode(",", array_keys((array)$custom_user_fields)); ?>'>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
   	<?php
	foreach ($custom_user_fields as $key => $result) {
		if(in_array($key, ['FirstName', 'LastName', 'Email']))
					  	continue;
	?>
	    <tr>
	        <th><label for="<?php echo $key;?>"><?php echo $key; ?></label></th>
	        <td>
	            <input type="text" name="<?php echo $key;?>" id="<?php echo $key;?>" value="<?php echo esc_attr( get_the_author_meta( $key, $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	<?php }?>
    </table>
	<?php 
}


	add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

	function save_extra_user_profile_fields( $user_id ) {
	    if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
	        return;
	    }
	    
	    if ( !current_user_can( 'edit_user', $user_id ) ) { 
	        return false; 
	    }
	    $custom_user_fields = explode(",",$_POST['custom_user_fields']);
	    //print_r($custom_user_fields);

	    foreach ($custom_user_fields as $key) {
	    	//echo $key.">>".$_POST[$key]."<br>";
	    	update_user_meta( $user_id, $key, $_POST[$key] );
		}
	    
	}


if (isset($_POST['sync'])) {

	$token = getToken();	
	$curl = curl_init();
    $host_url = "https://sparta-api.azurewebsites.net" ;
	curl_setopt_array($curl, array(
		CURLOPT_URL => $host_url."/api/event/LDRS22.96",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Authorization: {$token}"

		)
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	$results = json_decode($response);


	if ($err) {

		echo "cURL Error #:" . $err;

	} else {

		$users = get_users( array( 'fields' => array( 'user_email' ) ) );

	
		$site_emails = [];

		foreach($users as $user){
		    array_push($site_emails, $user->user_email);
		}

		foreach($results as $result){
			$email = $result->Email;
			$result->StartDate = substr($result->StartDate, 0, strpos($result->StartDate, "T"));
			$result->EndDate = substr($result->EndDate, 0, strpos($result->EndDate, "T"));

			if(in_array($email, $site_emails))
				continue;
			
			$role = 'um_notemployee';
			if($result->IsEmployee) {
				$role = 'um_employee-day-2';
				if($result->StartDate == $result->EndDate) {
					$role = 'um_employee-day-1';
				}
			}

			$user_cred = array(
							'role'					=> $role,
						    'user_pass'             => wp_generate_password( 5, false ),
						    'user_login'            => substr($email, 0, strpos($email, "@")),
						    'first_name'            => $result->FirstName,   
						    'last_name'             => $result->LastName,
						    'user_email'			=> $email
						);

			$user_id = wp_insert_user( $user_cred );
			//echo '<h3>' . "User created with ID: {$user_id}." . '</h3>';
 
			// On success.
			if ( ! is_wp_error( $user_id ) ) {
			    foreach ($result as $key => $value) {
					if(in_array($key, ['FirstName', 'LastName', 'Email']))
					  	continue;

					if(in_array($key, ['RSVP', 'IsEmployee', 'HotelEligible', 'NeedLodging']) )
						$value = $value == 1 ? 'true' : 'false';
			    	update_user_meta( $user_id, $key, $value );
			    
				}
			}
		}

	}
	
}



if (isset($_POST['pushsync'])) {

	$users = get_users( array( 'fields' => array( 'ID' ) ) );
	echo '<pre>';
	$i = 1;


// 	foreach($users as $single){
// // 		if($i === 50)
// // 			break;

// 		$user = get_user_meta ( $single->ID);
		
// 		if(($user['RSVP'][0] != 'false') && ($user['StartDate'][0] != $user['EndDate'][0])) {
// 			//continue;

// 		    $metas = array( 
// 			    'jobNumber'					=> 'LDRS22.96',
// 			    'PersonId'     				=> $user['PersonId'][0],
// 			    'IsEmployee'   				=> '# ' . $user['IsEmployee'][0] . ' #',
// 			    'NeedLodging'				=> '# ' . $user['NeedLodging'][0] . ' #',
// 			    'StartDate'    				=> $user['StartDate'][0],
// 			    'EndDate'     				=> $user['EndDate'][0],
// 			    'ConfirmedStartDate'    	=> ($user['ConfirmedStartDate'][0] === "") ? null : $user['ConfirmedStartDate'][0],
// 			    'ConfirmedEndDate'     		=> ($user['ConfirmedEndDate'][0] === "") ? null : $user['ConfirmedEndDate'][0],
// 		    	'RSVP'     					=> '# ' . $user['RSVP'][0] . ' #',
// 			    'LocationAttending'     	=> null,
// 			    'ReasonNotAttending'     	=> $user['ReasonNotAttending'][0],
// 			    'ReasonNotAttendingDetails'	=> ($user['ReasonNotAttendingDetails'][0]) ? $user['ReasonNotAttendingDetails'][0] : ""
// 			);

// 			$metas = str_replace(array('"# ', ' #"', '# ', ' #'), '', json_encode($metas));
// 			$mystring = "Data {$i}: {$metas} \n\n";
// 			file_put_contents('all_users.txt', $mystring, FILE_APPEND);
// 			$i++;
// 		}
// 	}






	$i = 1;
	foreach($users as $single){
		if($i > 900  && $i <= 950) {

			$user = get_user_meta ( $single->ID);
			
			if(($user['RSVP'][0] != 'false') && ($user['StartDate'][0] != $user['EndDate'][0])) {
				//continue;
			    $metas = array( 
				    'jobNumber'					=> 'LDRS22.96',
				    'PersonId'     				=> $user['PersonId'][0],
				    'IsEmployee'   				=> '# ' . $user['IsEmployee'][0] . ' #',
				    'NeedLodging'				=> '# ' . $user['NeedLodging'][0] . ' #',
				    'StartDate'    				=> $user['StartDate'][0],
				    'EndDate'     				=> $user['EndDate'][0],
				    'ConfirmedStartDate'    	=> ($user['ConfirmedStartDate'][0] === "") ? null : $user['ConfirmedStartDate'][0],
				    'ConfirmedEndDate'     		=> ($user['ConfirmedEndDate'][0] === "") ? null : $user['ConfirmedEndDate'][0],
			    	'RSVP'     					=> '# ' . $user['RSVP'][0] . ' #',
				    'LocationAttending'     	=> null,
				    'ReasonNotAttending'     	=> $user['ReasonNotAttending'][0],
				    'ReasonNotAttendingDetails'	=> ($user['ReasonNotAttendingDetails'][0]) ? $user['ReasonNotAttendingDetails'][0] : ""
				);

				$metas = str_replace(array('"# ', ' #"', '# ', ' #'), '', json_encode($metas));

				do {
					$result = send_updated_data($metas);
					if($result) {
						$mystring = "Data: {$metas} \n";
						$mystring .= "Result {$i}: {$result} \n\n";
						file_put_contents('result_log_19.txt', $mystring, FILE_APPEND);
					}
				} while(!$result);
			}
		}
		$i++;
	}
	echo '</pre>';

}


// Updating the User Meta data On Gravity Form Submission
add_action("gform_after_submission_2", "gravity_post_submission", 10, 2);
function gravity_post_submission ($entry, $form){
    
    $isAttending = rgar( $entry, '32' );
    $StartDate = rgar( $entry, '53' );
    $EndDate = rgar( $entry, '54' );
    $CStartDate = null;
    $CEndDate = null;

    
    if($isAttending != 'true'){

	    if($StartDate == $EndDate || $isAttending == 'both') {

	    	$CStartDate = $StartDate;
	    	$CEndDate = $EndDate;
	    	
	    }
	    else {

	    	$CStartDate = $isAttending;
	    	$CEndDate = $isAttending;

	    }

    }


    $notAttending = '1';
    if(rgar( $entry, '31' ) == '1')
    	$notAttending = '3';


    $metas = array( 
	    'jobNumber'					=> 'LDRS22.96',
	    'PersonId'     				=> rgar( $entry, '7' ),
	    'ManpowerReqDetailId'   	=> rgar( $entry, '8' ),
	    'IsEmployee'				=> '# ' . rgar( $entry, '5' ) . ' #',
	    'isAttending'   			=> $isAttending,
	    'StartDate'    				=> $StartDate,
	    'EndDate'     				=> $EndDate,
	    'ConfirmedStartDate'    	=> $CStartDate,
	    'ConfirmedEndDate'     		=> $CEndDate,
	    'HotelEligible'     		=> rgar( $entry, '13' ) ? '# true #' : '# false #',
	    'HotelRSVPCheckIn'     		=> rgar( $entry, '29' ),
	    'HotelRSVPCheckOut'    	 	=> rgar( $entry, '30' ),
	    'DispatchStatusId'     		=> '# ' . rgar( $entry, '18' ) . ' #',
	    'RSVP'     					=> '# true #',
	    'HotelAccomodations'		=> rgar( $entry, '33' ),
	    'NeedLodging'     			=> (rgar( $entry, '33' ) == 'Yes') ? '# true #' : '# false #',
	    'LocationRequested'     	=> null,
	    'LocationAttending'     	=> null,
	    'ReasonNotAttending'     	=> rgar( $entry, '31' ) ? '# ' . rgar( $entry, '31' ) . ' #' : null,
	    'TShirtSize'				=> rgar( $entry, '52' ),
	    'Class_1'					=> GFAPI::get_field( $form, '42' )->get_value_export( $entry, '42', true ),
	    'LastEmailSent'				=> date('Y-m-d'),
	    'Class_2'					=> GFAPI::get_field( $form, '45' )->get_value_export( $entry, '45', true ),
	    'Class_3'					=> GFAPI::get_field( $form, '46' )->get_value_export( $entry, '46', true ),
	    'Class_4'					=> GFAPI::get_field( $form, '47' )->get_value_export( $entry, '47', true ),
	    'Class_5'					=> GFAPI::get_field( $form, '48' )->get_value_export( $entry, '48', true ),
	    'Class_6'					=> GFAPI::get_field( $form, '49' )->get_value_export( $entry, '49', true ),
	    'PreferredName'				=> rgar( $entry, '58' ),
	    'CellNumber'				=> rgar( $entry, '59' ),
	    'ReasonNotAttendingDetails'		=> rgar( $entry, '66' )
	);




	foreach($metas as $key => $value) {
		$value = str_replace(array('# ', ' #'), '', $value);
	    update_user_meta( get_current_user_id(), $key, $value );
	}

	$metas['ReasonNotAttending'] = rgar( $entry, '31' ) ? '# ' . $notAttending . ' #' : null;

	
	foreach( array('LastEmailSent', 'HotelAccomodations', 'HotelEligible', 'HotelRSVPCheckIn', 'HotelRSVPCheckOut', 'ManpowerReqDetailId', 'DispatchStatusId', 'LocationRequested', 'isAttending', 'Class_1', 'Class_2', 'Class_3', 'Class_4', 'Class_5', 'Class_6', 'TShirtSize', 'PreferredName', 'CellNumber'  ) as $toUnset ){

		unset($metas[$toUnset]);

	}

	if($StartDate === $EndDate){
		unset($metas['NeedLodging']);
	}

	$metas = str_replace(array('"# ', ' #"', '# ', ' #'), '', json_encode($metas));

	send_updated_data($metas);
	echo "<pre class='d-none testing' style='white-space: pre-wrap; '>\nDATA TO SEND: \n{$metas}\n</pre>";
}





// On Form ID 1 Submission Sending Data Back to API
add_action( 'gform_after_submission_1', 'remove_form_entry' );
function remove_form_entry( $entry ) {
    GFAPI::delete_entry( $entry['id'] );
}


function send_updated_data($metas) {
	$token = getToken();
	$curl = curl_init();
	$host_url = "https://sparta-api.azurewebsites.net";

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $host_url . '/api/lodging/rsvp',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS =>$metas,
	  CURLOPT_HTTPHEADER => array(
	    "Authorization: {$token}",
	    'Content-Type: application/json'
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);
	
	$user = wp_get_current_user();
	if ( in_array( 'administrator', (array) $user->roles ) ) {
		if ($err)
			return($err);
			//echo "\nError:" . $err;
		else
			return($response);
			//echo "\nResponse:" . $response . "\n";
	}
}



// Populate RSVP STATUS FORM ReasonsNotAttending Field Dynamically Dropdown
add_filter('gform_pre_render_2', 'reason_not_attending');
function reason_not_attending($form) {

	$token = getToken();
	$curl = curl_init();

	$response = curl_exec($curl);

	curl_close($curl);

	$ress = json_decode($response);

	// foreach ($form['fields'] as &$field) {
	// 	if ($field->inputName == 'Reason-Not-Attending') {
 
 
	// 		$choices = [];
	// 		foreach($ress as $res){
	// 			$choices[] = ['text' => $res->Name, 'value' => $res->DeclineReasonId];
	// 		}
 
	// 		// Set choices to field
	// 		$field->choices = $choices;
	// 	}
	// }

	// Populating Attend Dates
	$sdate = get_user_meta(  get_current_user_id(), 'StartDate', true  );
	$sdate_text = 'Only ' . date("l, F  j, Y", strtotime($sdate));

	//$sdate = get_user_meta(  get_current_user_id(), 'EndDate', true  );
	$edate = get_user_meta(  get_current_user_id(), 'EndDate', true  );
	$edate_text = 'Only ' . date("l, F  j, Y", strtotime($edate));




	if($sdate == $edate){
		foreach ($form['fields'] as &$field) {
			if ($field->inputName == 'is-attending') {

				$options[] = ['text' => $sdate_text, 'value' => $sdate];
				$options[] = ['text' => "Not Attending", 'value' => "true"];
	 
				// Set choices to field
				$field->choices = $options;
			}
		}
	}
	else if ($sdate != $edate) {
		if(date("F", strtotime($sdate)) == date("F", strtotime($edate)))
			$bothdays = date("F ", strtotime($sdate)) . date("j-", strtotime($sdate)) . date("j", strtotime($edate)) . date(", Y", strtotime($sdate));
		else
			$bothdays = date("F j-", strtotime($sdate)) . date("F j", strtotime($edate)) . date(", Y", strtotime($sdate));
		foreach ($form['fields'] as &$field) {
			if ($field->inputName == 'is-attending') {

				$options[] = ['text' => $sdate_text, 'value' => $sdate];
				$options[] = ['text' => $edate_text, 'value' => $edate];
				$options[] = ['text' => "Both Days, " . $bothdays, 'value' => 'both'];
				$options[] = ['text' => "Not Attending", 'value' => "true"];
	 
				// Set choices to field
				$field->choices = $options;
			}
		}
	}
	//echo "{$sdate}\n";
	//echo $edate;
	return $form;
}

// Ajax Get All users
add_action( 'init', 'user_script_enqueuer' );

function user_script_enqueuer() {
   wp_register_script( "users_list_script", get_template_directory_uri() .'/assets/js/api_script.js', array('jquery') );
   wp_localize_script( 'users_list_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
   wp_enqueue_script( 'users_list_script' );

}

add_action("wp_ajax_api_users_list", "api_users_list");
add_action("wp_ajax_nopriv_api_users_list", "api_users_list");

function api_users_list() {

  if ( !wp_verify_nonce( $_REQUEST['nonce'], "users_list_nonce")) {
      exit("No naughty business please");
   }   


   	// List users
	$invitees = [];
	$search_filter = isset($_GET['filter_normal']) ? $_GET['filter_normal'] : '';
	$filter_company = isset($_GET['filter_company']) ? $_GET['filter_company'] : '';
	$active_invitees = isset($_GET['active_invitees']) ? $_GET['active_invitees'] : '';
	$data_sort = isset($_GET['data_sort']) ? $_GET['data_sort'] : 'asc';
	$sort_field = isset($_GET['sort_field']) ? $_GET['sort_field'] : 'last_name';
	$filter_per_page = isset($_GET['filter_per_page']) && !empty($_GET['filter_per_page'])  ? $_GET['filter_per_page'] : 10;
	
	
	if($active_invitees != '')
	{
		$user_args = array(
            'meta_query' => array(
            	'relation' => 'AND',
			    array( 
			        'key' => 'RSVP',
		            'value' =>  'true',
		            'compare' => '='
			    ),
            	array(
            		'relation' => 'OR',
                )
            )
        );
	}
	else{
		$user_args = array(
            'meta_query' => array(
            	array(
            		'relation' => 'OR',
                )
            )
        );
	}

	if($search_filter != ''){
		if($user_args['meta_query'][0]['relation'] === "OR"){
			$user_args['meta_query']['relation'] = "AND";
			$user_args['meta_query'][] =  array(
					                    	'relation' => 'OR',
					                    	array(
						                        'key' => 'MiddleName',
						                        'value' =>  $search_filter.'*',
						                        'compare' => 'REGEXP'
					                    	),
					                    	array(
						                        'key' => 'first_name',
						                        'value' =>  $search_filter.'*',
						                        'compare' => 'REGEXP'
					                    	),
					                    	array(
						                        'key' => 'last_name',
						                        'value' =>  $search_filter.'*',
						                        'compare' => 'REGEXP'
					                    	),
					                    	array(
						                        'key' => 'email_field',
						                        'value' =>  $search_filter.'*',
						                        'compare' => 'REGEXP'
					                    	),

					                      );
		}
		else{

			$user_args['meta_query'][1][] =  array(
						                    	'relation' => 'OR',
						                    	array(
							                        'key' => 'nickname',
							                        'value' =>  $search_filter.'*',
							                        'compare' => 'REGEXP'
						                    	),
						                    	array(
							                        'key' => 'first_name',
							                        'value' =>  $search_filter.'*',
							                        'compare' => 'REGEXP'
						                    	),
						                    	array(
							                        'key' => 'last_name',
							                        'value' =>  $search_filter.'*',
							                        'compare' => 'REGEXP'
						                    	),
						                    	array(
							                        'key' => 'email_field',
							                        'value' =>  $search_filter.'*',
							                        'compare' => 'REGEXP'
						                    	),

						                      );
			
		}
	}

	if($filter_company != ''){
		if($user_args['meta_query'][0]['relation'] == "OR"){
			$user_args['meta_query']['relation'] = "AND";
			$user_args['meta_query'][] =  array(
						                        'key' => 'Company',
						                        'value' =>  $filter_company.'*',
						                        'compare' => 'REGEXP'
						                    );
			//echo "Compnay OR COndtiont";
		}
		else{
			$user_args['meta_query'][1][] =  array(
						                        'key' => 'Company',
						                        'value' =>  $filter_company.'*',
						                        'compare' => 'REGEXP'
						                    );
		}
	}


	$userArray = get_users( 
		$user_args
		// array (
		//     'meta_value'   => $search_filter,
		//     'meta_compare' => 'LIKE',
		//     'orderby'      => 'meta_value',
		//     'order'        => 'ASC',
		// )
	);
	/*$args = array(
		'meta_query' => array(
		    array(
		        'value' => sprintf(':"%s";', $search_filter),
		        'compare' => 'LIKE'
		    )
		)
	);

	$userArray = new WP_User_Query( $args );*/

	// Array of WP_User objects.
	foreach ( $userArray as $user ) {
	     array_push($invitees, get_user_meta( $user->ID ) + array('email_field'=>$user->user_email));
	}
	if($data_sort == 'asc')
	{
		usort($invitees, function ($item1, $item2) use ($sort_field){
			if(is_array($item1[$sort_field])){
				return strtolower($item1[$sort_field][0]) <=> strtolower($item2[$sort_field][0]);
			}
			else
		    	return strtolower($item1[$sort_field]) <=> strtolower($item2[$sort_field]);
		});
	}
	else{
		usort($invitees, function ($item1, $item2) use ($sort_field){
			if(is_array($item1[$sort_field]))
		    	return strtolower($item2[$sort_field][0]) <=> strtolower($item1[$sort_field][0]);
		    else
		    	return strtolower($item2[$sort_field]) <=> strtolower($item1[$sort_field]);
		});
	}
	

	$limit = $filter_per_page;
	$total = count($invitees);
	$pages = ceil($total / $limit);
	$result = ceil($total / $limit);

	$current = isset($_GET['pageno']) ? $_GET['pageno'] : 1;
	$next = $current < $pages ? $current + 1 : $current;
	$previous = $current > 1 ? $current - 1 : $current;

	$offset = ($current - 1) * $limit;
	$invitees = array_slice($invitees, $offset, $limit);
	//echo "123";print_r($invitees);


	global $wp;

	$raw_url =  home_url( $wp->request ); 

	$pos = strpos($current_url , '&pageno');

	$curr_url = substr($raw_url, 0, $pos);

	ob_start();
	?>
	  <?php 
	  	if( count( $invitees ) > 0 )
	  	{
		  foreach (array_slice($invitees, $page*$per_page, $per_page) as $invitee) {
		  ?>
		  <tr>
		    <td><?php echo $invitee['last_name'][0] . ', ' . $invitee['first_name'][0]. ' ' . $invitee['MiddleName'][0];?></td>
		    <td><?php echo $invitee['email_field'];?></td>
		    <td><?php echo $invitee['Company'][0];?></td>
		    <td>Yes</td>
		    <td><?php if($invitee['RSVP'][0] == 'true') echo "Yes"; else echo 'Not Specified';?></td>
		    <td><?php if($invitee['isAttending'][0] != 'true') echo 'No'; else echo 'Yes'; ?></td>
		    <td><?php echo $invitee['NeedLodging'][0];?></td>
		    <td><?php echo $invitee['TShirtSize'][0];?></td>
		    <td><?php echo $invitee['PreferredName'][0];?></td>
		  </tr>
		  <?php 
		  }
		}
		else{
			echo "<tr><td colspan='9'>No record found.</td></tr>";
		}
		?>

	<?php
	$result1 =  ob_get_contents();//['table']
	ob_end_clean();

	// Create Pagination HTML
	ob_start();
	?>
	<div class="pagin-btns">
		<nav aria-label="Page navigation example">
			<ul class="pagination my-0">
				<li class="page-item">
				  <a data-page="<?php echo '1'; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . '1'; ?>" aria-label="Previous">
				    <span aria-hidden="true">&laquo;</span>
				    <span class="sr-only">Previous</span>
				  </a>
				</li>
				<li class="page-item"><a data-page="<?php echo $previous; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $previous; ?>">&lt;</a></li>
				<li class="page-item active"><a data-page="" class="page-link" href="#"><?php echo $current; ?></a></li>
				<li class="page-item"><a data-page="<?php echo $next; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $next; ?>">&gt;</a></li>
				<li class="page-item">
				  <a data-page="<?php echo $pages; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $pages; ?>" aria-label="Next">
				    <span aria-hidden="true">&raquo;</span>
				    <span class="sr-only">Next</span>
				  </a>
				</li>
			</ul>
		</nav>
	</div>
	<?php
	$result2 = ob_get_contents();//['pagination']
	ob_end_clean();
	//$result['table'] = htmlentities(stripslashes(utf8_encode($result1)), ENT_QUOTES);
	//$result['pagination'] = htmlentities(stripslashes(utf8_encode($result2)), ENT_QUOTES);
	//echo "1bac";//print_r($result1);
	echo json_encode(array(
		'type' => 'success',
	    'table' => $result1,
	    'pagination' => $result2
	));
    die();
}


// Gravity Forms User Role Field Dynamic
add_filter('gform_field_value_user_role', 'gform_populate_user_role');
function gform_populate_user_role($value){
    $user = wp_get_current_user();
    $role = $user->roles;
    return reset($role);
}




// WordPress Default Forget Password Email Template
// add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
// function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

//     // Start with the default content.
//     $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
//     $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
//     /* translators: %s: site name */
//     $message .= sprintf( __( 'Site Name: %s' ), $site_name ) . "\r\n\r\n";
//     /* translators: %s: user login */
//     $message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
//     $message .= __( '' ) . "\r\n\r\n";
//     $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
//     $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";

//     /*
//      * If the problem persists with this filter, remove
//      * the last line above and use the line below by
//      * removing "//" (which comments it out) and hard
//      * coding the domain to your site, thus avoiding
//      * the network_site_url() function.
//      */
//     // $message .= '<http://yoursite.com/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login ) . ">\r\n";

//     // Return the filtered message.
//     return $message;

// }


// Sending the SMS To Users
add_action("gform_after_submission_3", "send_sms_to_users", 10, 2);

function send_sms_to_users($entry, $form) {

	$token = getToken();
	$curl = curl_init();
	$host_url = "https://sparta-api.azurewebsites.net";
	$sms_message = rgar( $entry, '1' );
	$user_role = rgar( $entry, '2' );


 	$allmetas = array();
	$users = get_users(array("role" => $user_role));

	foreach($users as $user){
        $usermeta = get_user_meta( $user->ID);
        $allmetas[] = $usermeta['CellPhone'][0];
    }

    $numInStr = '"' . implode('", "', $allmetas) . '"';


	curl_setopt_array($curl, array(
	  CURLOPT_URL => $host_url . '/api/zipwhip/message/send',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS =>'{ "SenderPhone": "2816801641", "ReceiverPhones": [' . $numInStr . '], "Body": "' . $sms_message . '"',
	  CURLOPT_HTTPHEADER => array(
	    "Authorization: {$token}",
	    'Content-Type: application/json'
	  ),
	));

	$response = curl_exec($curl);

	curl_close($curl);

}
