<?php /* Template Name: Login Page Template */ 

 get_header("two");

?>

	<div class="row full-width-section mx-0 login-text">
		<div class="col-md-10 mx-auto my-auto text-center">
			<p class="sub-heading white-clr">Login</p>
			<h1 class="heading bold text-uppercase white-clr">WELCOME TO UPS IGNITE</h1>
			<?php 
				echo do_shortcode( '[ultimatemember form_id="13"]' );
			?>
			<p class="login-bottom" style="text-align: center; font-size:20px;">Need assistance, <a href="https://ignite.universalplant.com/contact/">get in touch</a>.</p>
		</div>
		
	</div>




<?php  get_footer("two"); ?>