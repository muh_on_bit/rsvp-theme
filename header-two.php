<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

</head>

<?php
	$browser = new Wolfcast\BrowserDetection();
	if($browser->getPlatform() == Wolfcast\BrowserDetection::PLATFORM_IOS):
		$ios = 'ios';
	else:
		$ios = '';
	endif;
?>

<body <?php body_class($ios); ?>>
<div id="page" class="site">
	<?php do_action( 'before' ); ?>
	<!-- #masthead .site-header -->

	<div id="main">
