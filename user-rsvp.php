<?php /* Template Name: User RSVP Form */ 

get_header();


$user_id = get_current_user_id(); 

$all_meta_for_user = get_user_meta( $user_id );
	$userdetails = [];
	foreach($all_meta_for_user as $key => $userdetail) {

		$userdetails[$key] = $userdetail[0];

	}


	$user_info = get_userdata($user_id);
	$mailadresje = $user_info->user_email;



?>





<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<?php 
				echo do_shortcode( "[gravityform id='2' title='false' description='false' ajax='true' field_values='fname={$userdetails['first_name']}&lname={$userdetails['last_name']}&mname={$userdetails['MiddleName']}&user-email={$mailadresje}&user-company={$userdetails['Company']}&is-employee={$userdetails['IsEmployee']}&person-id={$userdetails['PersonId']}&Manpower-Req-Detail-Id={$userdetails['ManpowerReqDetailId']}&Start-Date={$userdetails['StartDate']}&End-Date={$userdetails['EndDate']}&Confirmed-Start-Date={$userdetails['ConfirmedStartDate']}&Confirmed-End-Date={$userdetails['ConfirmedEndDate']}&Hotel-Eligible={$userdetails['HotelEligible']}&Approved-CheckIn={$userdetails['ApprovedCheckIn']}&Approved-CheckOut={$userdetails['ApprovedCheckOut']}&Hotel-RSVP-CheckIn={$userdetails['HotelRSVPCheckIn']}&Hotel-RSVP-CheckOut={$userdetails['HotelRSVPCheckOut']}&Dispatch-StatusId={$userdetails['DispatchStatusId']}&RSVP={$userdetails['RSVP']}&Need-Lodging={$userdetails['NeedLodging']}&Location-Requested={$userdetails['LocationRequested']}&Location-Attending={$userdetails['LocationAttending']}&Reason-Not-Attending={$userdetails['ReasonNotAttending']}']" );
			?>
		</div>
	</div>
</div>




<?php get_footer(); ?>