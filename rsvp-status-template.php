<?php /* Template Name: RSVP Status Form */ 

get_header();

$Classes = array(
			1 => 'Moving from Telling to Asking', 2 => 'Effective Teamwork', 3 => 'Leading and Managing People', 4 => 'UPS Service Lines', 5 => 'Field Finances 101', 6 => 'Negotiations: They’re Not Just for Contracts', 7 => 'Leadership Skills Learned the Hard Way'
		);


$user_id = get_current_user_id(); 

$all_meta_for_user = get_user_meta( $user_id );
$userdetails = [];
foreach($all_meta_for_user as $key => $userdetail) {

	$userdetails[$key] = $userdetail[0];

}

$userdetails['Class_1'] = array_search($userdetails['Class_1'], $Classes);
$userdetails['Class_2'] = array_search($userdetails['Class_2'], $Classes);
$userdetails['Class_3'] = array_search($userdetails['Class_3'], $Classes);
$userdetails['Class_4'] = array_search($userdetails['Class_4'], $Classes);
$userdetails['Class_5'] = array_search($userdetails['Class_5'], $Classes);
$userdetails['Class_6'] = array_search($userdetails['Class_6'], $Classes);
$reasonNotAtt = $userdetails['ReasonNotAttending'];
if ($userdetails['NeedLodging'] === 'true' ) {
	$needLodging = 'Yes';
}
else {
	$needLodging = 'No';
}
echo "<div class='d-none'>". $userdetails['NeedLodging']. "</div>";
// $field = GFAPI::get_field( $form, 17 );
// $field_label = $field->label;

$user_info = get_userdata($user_id);
$mailadresje = $user_info->user_email;


?>
<div class="container custom-page-container">
			<div class="row w-100">
				<div class="left-section col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
					<ul class="custom-page-account-section">
						<li><a href="/account-page/">My Account</a></li>
						<?php
							$user = wp_get_current_user();
							if ( in_array( 'um_employee-day-1', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list notification-icon"><a href="#user-day-one"> <u>Welcome Packet</u></a></li>
								<?php
							}
							if ( in_array( 'um_notemployee', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list notification-icon"><a href="#not-employee"> <u>Welcome Packet</u></a></li>
								<?php
							}
							if ( in_array( 'um_employee-day-2', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list notification-icon"><a href="#user-day-two"> <u>Welcome Packet</u></a></li>
								<?php
							}
							if ( in_array( 'administrator', (array) $user->roles ) ) {
								?>
									<li class="rsvp-list notification-icon"><a href="#admin"> <u>Welcome Packet</u></a></li>
								<?php
							}
						?>
						<li class="rsvp-list notification-icon test123"><a href="/rsvp-status/"> <u>RSVP status</u></a></li>
						
					</ul>
				</div>
				<div class="right-section col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<div class="cust-page-title"> <?php the_title(); ?></div>
					 <?php 
				echo do_shortcode( "[gravityform id='2' title='false' description='false' ajax='true' field_values='is-attending={$userdetails['isAttending']}&is-employee={$userdetails['IsEmployee']}&person-id={$userdetails['PersonId']}&Manpower-Req-Detail-Id={$userdetails['ManpowerReqDetailId']}&Start-Date={$userdetails['StartDate']}&End-Date={$userdetails['EndDate']}&Confirmed-Start-Date={$userdetails['ConfirmedStartDate']}&Confirmed-End-Date={$userdetails['ConfirmedEndDate']}&Approved-CheckIn={$userdetails['ApprovedCheckIn']}&Approved-CheckOut={$userdetails['ApprovedCheckOut']}&Reason-Not-Attending={$userdetails['ReasonNotAttending']}&tshirt-size={$userdetails['TShirtSize']}&tshirt-size={$userdetails['TShirtSize']}&Class_1={$userdetails['Class_1']}&Class_2={$userdetails['Class_2']}&Class_3={$userdetails['Class_3']}&Class_4={$userdetails['Class_4']}&Class_5={$userdetails['Class_5']}&Class_6={$userdetails['Class_6']}&prefferred-name={$userdetails['PreferredName']}&cell-number={$userdetails['CellNumber']}&need_Lodging={$needLodging}&decline-reason-details={$userdetails['DeclineReasonDetails']}']" );
			?>
					 
				</div>
			</div>
		</div>


<?php get_footer(); ?>