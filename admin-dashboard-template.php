<?php /* Template Name: Admin Dashboard Template */ 

get_header();

?>
<div class="full-width-container">
	<div class="container p-0 custom-page-container dashboard-cont">
		<div class="row mx-0 w-100">
			<div class="col-12 p-0">
				<div class="content_wrapper">
					<div class="section_header">
						<a href="#" class="btn grey-btn btn-back text-white"><span style="transform: rotate(180deg); display: inline-block; margin-right: 5px;">&#10132;</span>Back</a>
						<h2 class="">UPS Ignite 2021</h2>
						<div class="buttons-wrap">
							<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="text-center">
							  	<button type="submit" class="btn btn-primary btn-sync" name="sync">Sync Invitees</button>
							</form>



							<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="text-center">
							  	<button type="submit" class="btn btn-primary btn-sync" name="pushsync">Push</button>
							</form>


							<!-- <a href="#" class="btn btn-primary btn-sync">Sync Invitees</a> -->
							<a href="#" class="btn btn-success btn-event">Edit Event</a>
						</div>
					</div>
					<?php
					$token = getToken();	
					$curl = curl_init();
					$host_url = "https://sparta-api.azurewebsites.net" ;
					curl_setopt_array($curl, array(
						CURLOPT_URL => $host_url."/api/event/LDRS22.96",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => array(
							"Authorization: {$token}"
						)
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);

					$results = json_decode($response);
// 					foreach($results as $result){
// 						$startDate = $result->StartDate;
// 						$endDate = $result->EndDate;
// 					}
					$startDate = $results[0]->StartDate;
					$endDate = $results[0]->EndDate;
					$startDate_trim = strstr($startDate, 'T', true);
					$endDate_trim = strstr($endDate, 'T', true);
					?>
					<div class="section_inner_wrapper">
						<div class="section_information">
							<h3 class="h3 border-bottom">Event Information</h3>
							<ul class="uo-list custom-list">
								<li class="list-item"><span class="bold">Description: </span></li>
								<li class="list-item"><span class="bold">Year: </span>2021</li>
								<li class="list-item"><span class="bold">Day 1: </span><?php echo $startDate_trim; ?></li>
								<li class="list-item"><span class="bold">Day 2: </span><?php echo $endDate_trim; ?></li>
								<li class="list-item"><span class="bold">API ID: </span>LDRS22.96</li>
							</ul>
						</div>
					</div>
					<form action="" method="post" id="search-user">
						<div class="section_inner_wrapper">
							<div class="section_information">
								<h3 class="h3 border-bottom">Event Invitees</h3>
								<?php $nonce = wp_create_nonce("users_list_nonce");?>
								<input type="hidden" value="<?php echo $nonce;?>" name="wp_nonce">
								<input type="hidden" value="" name="data_sort">
								<input type="hidden" value="last_name" name="sort_field">
								<input type="hidden" value="1" name="pageno">
								<div class="filters_wrap">
									<div class="filter-item">
										<form class="d-flex">
											<label>Filter</label>
											<input type="text" name="filter-normal">
											<button class="btn grey-btn">Reset filter</button>
										</form>
									</div>
									<div class="filter-item">
										<form class="d-flex">
											<label>Company</label>
											<input type="text" name="filter_company">
											<button class="btn grey-btn">Reset filter</button>
										</form>
									</div>
									<div class="filter-item">
										<form class="d-flex">
											<label>Invitees per page</label>
											<input type="number" name="filter_per_page">
										</form>
									</div>
								</div>
							</div>
							<?php
							$invitees = [];
							$userArray = get_users( 'orderby=nicename' );
								// Array of WP_User objects.
								
								
								foreach ( $userArray as $user ) {
								     array_push($invitees, get_user_meta( $user->ID ) + array('email_field'=>$user->user_email));
								}

	                        $limit = 10;
	                        $total = count($invitees);
	                        $pages = ceil($total / $limit);
	                        $result = ceil($total / $limit);

	                        $current = isset($_GET['pageno']) ? $_GET['pageno'] : 1;
	                        $next = $current < $pages ? $current + 1 : $current;
	                        $previous = $current > 1 ? $current - 1 : $current;

	                        $offset = ($current - 1) * $limit;
	                        $invitees = array_slice($invitees, $offset, $limit);



							global $wp;

							$raw_url =  home_url( $wp->request ); 

							$pos = strpos($current_url , '&pageno');

							$curr_url = substr($raw_url, 0, $pos);

							?>
							<div class="navigation">
								<div class="pagination_wrap">
									<div class="pagination">
										<div class="d-flex">
											<div class="pagin-btns">
											<nav aria-label="Page navigation example">
												<ul class="pagination my-0">
													<li class="page-item">
													  <a class="page-link" data-page="<?php echo $previous; ?>" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $previous; ?>" aria-label="Previous">
													    <span aria-hidden="true">&laquo;</span>
													    <span class="sr-only">Previous</span>
													  </a>
													</li>
													<li class="page-item"><a class="page-link" data-page="<?php echo '1'; ?>" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . '1'; ?>">&lt;</a></li>
													<li class="page-item active"><a data-page="" class="page-link" href="#"><?php echo $current; ?></a></li>
													<li class="page-item"><a data-page="<?php echo $next; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $next; ?>">&gt;</a></li>
													<li class="page-item">
													  <a data-page="<?php echo $pages; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $pages; ?>" aria-label="Next">
													    <span aria-hidden="true">&raquo;</span>
													    <span class="sr-only">Next</span>
													  </a>
													</li>
												</ul>
											</nav>
									</div>
										</div>
									</div>
									<div class="user_check">
										<div class="invitee_check">
											<input type="checkbox" id="active_invitees" name="active_invitees" value="true">
											<label for="active_invitees">Show only active Invitees</label>
										</div>
									</div>
								</div>
							</div>
							<div class="filter_btns pb-3">
								<!-- <div class="btns-group-left">
									<a href="#" class="btn grey-btn">Select all</a>
									<a href="#" class="btn grey-btn">Clear selection</a>
								</div> -->
								<!-- <div id="dropdown_" class="dropdown">
								    <button class="btn grey-btn" type="button" data-toggle="dropdown">Send Mass Email
								    <span class="caret"></span></button>
								    <ul class="dropdown-menu">
								      <li><a href="#">Option 1</a></li>
								      <li><a href="#">Option 2</a></li>
								      <li><a href="#">Option 3</a></li>
								    </ul>
								</div> -->
							</div>
							<hr>
							<div class="data-table-wrap">
								<div id="api_users_list" class="data-table">

									<table>
										<thead>
									  		<tr>
									    		<th><button type="button" class="arrow-sort active" data-name="last_name"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="36.678px" height="36.678px" viewBox="0 0 36.678 36.678" style="enable-background:new 0 0 36.678 36.678;" xml:space="preserve">
													<g>
														<path d="M29.694,20.074c0.087,0.16,0.08,0.356-0.02,0.512L19.393,36.449c-0.089,0.139-0.241,0.224-0.406,0.229   c-0.004,0-0.009,0-0.014,0c-0.159,0-0.31-0.074-0.403-0.206L6.997,20.609c-0.111-0.152-0.127-0.354-0.042-0.521   s0.258-0.273,0.446-0.273h21.855C29.439,19.814,29.608,19.914,29.694,20.074z M7.401,16.864h21.855c0.007,0,0.013,0,0.02,0   c0.276,0,0.5-0.224,0.5-0.5c0-0.156-0.069-0.295-0.184-0.387L18.086,0.205C17.989,0.073,17.838,0.009,17.669,0   c-0.165,0.005-0.315,0.09-0.406,0.228L6.982,16.092c-0.101,0.154-0.107,0.35-0.021,0.511C7.05,16.764,7.218,16.864,7.401,16.864z" style="&#10;    fill: #ccc;&#10;    stroke: #ccc;&#10;"/>
													</g>
													</svg></button>Name</th>
									    		<th><button data-name="email_field" type="button" class="arrow-sort"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="36.678px" height="36.678px" viewBox="0 0 36.678 36.678" style="enable-background:new 0 0 36.678 36.678;" xml:space="preserve">
													<g>
														<path d="M29.694,20.074c0.087,0.16,0.08,0.356-0.02,0.512L19.393,36.449c-0.089,0.139-0.241,0.224-0.406,0.229   c-0.004,0-0.009,0-0.014,0c-0.159,0-0.31-0.074-0.403-0.206L6.997,20.609c-0.111-0.152-0.127-0.354-0.042-0.521   s0.258-0.273,0.446-0.273h21.855C29.439,19.814,29.608,19.914,29.694,20.074z M7.401,16.864h21.855c0.007,0,0.013,0,0.02,0   c0.276,0,0.5-0.224,0.5-0.5c0-0.156-0.069-0.295-0.184-0.387L18.086,0.205C17.989,0.073,17.838,0.009,17.669,0   c-0.165,0.005-0.315,0.09-0.406,0.228L6.982,16.092c-0.101,0.154-0.107,0.35-0.021,0.511C7.05,16.764,7.218,16.864,7.401,16.864z" style="&#10;    fill: #ccc;&#10;    stroke: #ccc;&#10;"/>
													</g>
													</svg></button>Email</th>
									    		<th><button type="button" class="arrow-sort" data-name="Company"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="36.678px" height="36.678px" viewBox="0 0 36.678 36.678" style="enable-background:new 0 0 36.678 36.678;" xml:space="preserve">
													<g>
														<path d="M29.694,20.074c0.087,0.16,0.08,0.356-0.02,0.512L19.393,36.449c-0.089,0.139-0.241,0.224-0.406,0.229   c-0.004,0-0.009,0-0.014,0c-0.159,0-0.31-0.074-0.403-0.206L6.997,20.609c-0.111-0.152-0.127-0.354-0.042-0.521   s0.258-0.273,0.446-0.273h21.855C29.439,19.814,29.608,19.914,29.694,20.074z M7.401,16.864h21.855c0.007,0,0.013,0,0.02,0   c0.276,0,0.5-0.224,0.5-0.5c0-0.156-0.069-0.295-0.184-0.387L18.086,0.205C17.989,0.073,17.838,0.009,17.669,0   c-0.165,0.005-0.315,0.09-0.406,0.228L6.982,16.092c-0.101,0.154-0.107,0.35-0.021,0.511C7.05,16.764,7.218,16.864,7.401,16.864z" style="&#10;    fill: #ccc;&#10;    stroke: #ccc;&#10;"/>
													</g>
													</svg></button>Company</th>
											    <th>Invited</th>
											    <th>RSVPd</th>
											    <th>Attending</th>
											    <th>Hotel (Y/N)</th>
											    <th>TShirt (Size)</th>
											    <th>Preferred Name</th>
									  		</tr>
									  	</thead>
									    <tbody>
									  <?php foreach (array_slice($invitees, $page*$per_page, $per_page) as $invitee) {
									  ?>
									  <tr>
									    <td><?php echo $invitee['last_name'][0] . ', ' . $invitee['first_name'][0] . ' ' . $invitee['MiddleName'][0];?></td>
									    <td><?php echo $invitee['email_field'];?></td>
									    <td><?php echo $invitee['Company'][0];?></td>
									    <td>Yes</td>
									    <td><?php if($invitee['RSVP'][0] == 'true') echo 'Yes'; else echo 'Not Specified';?></td>
									    <td><?php if($invitee['RSVP'][0] == 'false') echo 'Not Specified'; else if($invitee['isAttending'][0] != 'true') echo 'Yes'; else echo 'No'; ?></td>
									    <td><?php echo $invitee['NeedLodging'][0];?></td>
									    <td><?php echo $invitee['TShirtSize'][0];?></td>
									    <td><?php echo $invitee['PreferredName'][0];?></td>
									  </tr>
									  <?php }?>
									  </tbody>
									</table>
								</div>
							</div>
							<div class="pagination">
								<div class="d-flex">
									<div class="pagin-btns">
											<nav aria-label="Page navigation example">
												<ul class="pagination my-0">
													<li class="page-item">
													  <a class="page-link" data-page="<?php echo $previous; ?>" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $previous; ?>" aria-label="Previous">
													    <span aria-hidden="true">&laquo;</span>
													    <span class="sr-only">Previous</span>
													  </a>
													</li>
													<li class="page-item"><a class="page-link" data-page="<?php echo '1'; ?>" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . '1'; ?>">&lt;</a></li>
													<li class="page-item active"><a data-page="" class="page-link" href="#"><?php echo $current; ?></a></li>
													<li class="page-item"><a data-page="<?php echo $next; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $next; ?>">&gt;</a></li>
													<li class="page-item">
													  <a data-page="<?php echo $pages; ?>" class="page-link" href="<?php echo $curr_url . '/admin-dashboard/?pageno=' . $pages; ?>" aria-label="Next">
													    <span aria-hidden="true">&raquo;</span>
													    <span class="sr-only">Next</span>
													  </a>
													</li>
												</ul>
											</nav>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
  $(".dropdown").on("hide.bs.dropdown", function(){
    $(".dropdown .btn").html('Send Mass Email <span class="caret"></span>');
  });
  $(".dropdown").on("show.bs.dropdown", function(){
    $(".dropdown .btn").html('Send Mass Email <span class="caret caret-up"></span>');
  });
});
</script>

<?php get_footer(); ?>