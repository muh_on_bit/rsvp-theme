<?php /* Template Name: Send SMS to Users */ 

get_header();

$user_id = get_current_user_id(); 

$all_meta_for_user = get_user_meta( $user_id );
$userdetails = [];
foreach($all_meta_for_user as $key => $userdetail) {

	$userdetails[$key] = $userdetail[0];

}

$userdetails['Class_1'] = array_search($userdetails['Class_1'], $Classes);
$userdetails['Class_2'] = array_search($userdetails['Class_2'], $Classes);
$userdetails['Class_3'] = array_search($userdetails['Class_3'], $Classes);
$userdetails['Class_4'] = array_search($userdetails['Class_4'], $Classes);
$userdetails['Class_5'] = array_search($userdetails['Class_5'], $Classes);
$userdetails['Class_6'] = array_search($userdetails['Class_6'], $Classes);
$reasonNotAtt = $userdetails['ReasonNotAttending'];

$user_info = get_userdata($user_id);
$mailadresje = $user_info->user_email;


?>
<div class="container mt-5 py-5">
			<div class="row w-100">

				<div class="right-section mt-5 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<?php 
						echo do_shortcode( "[gravityform id='3']" );
					?>
				</div>
			</div>
		</div>


<?php get_footer(); ?>