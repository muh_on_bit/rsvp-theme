"use strict";

jQuery(function ($) {

  // List Users
  $("#search-user").on( 'change input', function(e) {
  		var filter_normal = $("#search-user input[name=filter-normal]").val();//$(this).attr("data-post_id");
  		var filter_company = $("#search-user input[name=filter_company]").val();
  		var filter_per_page = $("#search-user input[name=filter_per_page]").val();
  		var sort_field =  $("#search-user input[name=sort_field]").val();
  		var data_sort =  $("#search-user input[name=data_sort]").val();
      var pageno =  $("#search-user input[name=pageno]").val();
	  	var nonce = $("#search-user input[name=wp_nonce]").val();
	  	var active_invitees = '';
	  	if($('#active_invitees').is(':checked')){
	  		active_invitees = true;
	  	}

		$.ajax({
		 type : "get",
		 dataType : "json",
		 url : myAjax.ajaxurl,
		 data : {action: "api_users_list", pageno : pageno, sort_field : sort_field, data_sort : data_sort, filter_per_page : filter_per_page, active_invitees : active_invitees, filter_normal : filter_normal, filter_company: filter_company, nonce: nonce},
		 success: function(response) {
		    if(response.type == "success") {
		       $("#api_users_list table").find('tbody').empty().append(response.table);
		       $(".pagination .d-flex").html(response.pagination);
		    }
		    else {
		    	$("#api_users_list table").find('tbody').empty().append("No users found.");
		    	$(".pagination .d-flex").html('');
		    }
		 }
		});  
  });


  $("#search-user .arrow-sort").on( 'click', function(e) {
  	 $("#search-user .arrow-sort").removeClass('active');
  	 $(this).addClass('active');
  	 if($("#search-user input[name=data_sort]").val() == "" || $("#search-user input[name=data_sort]").val() == "desc"){
  	 	$("#search-user input[name=data_sort]").val('asc');
  	 }
  	 else{
  	 	$("#search-user input[name=data_sort]").val('desc');
  	 }
  	 var active_sort_field = $(this).attr('data-name');
  	 console.log(active_sort_field);
  	 $("#search-user input[name=sort_field]").val(active_sort_field);
  	 $("#search-user").trigger('change');
  });

  $('body').on( 'click', "#search-user .pagination .page-link", function(e) {
     e.preventDefault();
     $("#search-user .pagination .page-item").removeClass('active');
     $(this).parent('.page-item').addClass('active');
     var data_page = $(this).attr('data-page');
     console.log(data_page);
     $("#search-user input[name=pageno]").val(data_page);
     $("#search-user").trigger('change');
  });

});