jQuery( function( $ ) {
	$( '#mobile-menu' ).on( 'click', function() {
		$( '.main-navigation' ).toggleClass( 'active' );
	});

	if ( $( 'body' ).hasClass( 'ios' ) ) {
		$( 'INSERT SELECTOR HERE' ).on( 'click touchend', function( e ) {
		    var el = $( this );
		    var link = el.attr( 'href' );
		    window.location = link;
		  });
	}
});
