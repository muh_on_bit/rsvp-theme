<?php
/*======================
	=Init
========================*/
add_action( 'init', 'big_splash_init' );
function big_splash_init() {
	//add_post_type_support( 'page', 'excerpt' );
	
	// REMOVE WP EMOJI
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	 
	/*======================
	=Register Post Type

	$singular_name = "Test";
	$plural_name = "Tests";
	$slug = "test";
	$post_name = "test";
	
	$labels = array(
		'name' => $plural_name,
		'singular_name' => $singular_name,
		'add_new' => 'Add New',
		'add_new_item' => 'Add New '.$singular_name,
		'edit_item' => 'Edit '.$singular_name,
		'new_item' => 'New '.$singular_name,
		'all_items' => 'All '.$plural_name,
		'view_item' => 'View '.$singular_name,
		'search_items' => 'Search '.$plural_name,
		'not_found' =>  'No '.$plural_name.' found',
		'not_found_in_trash' => 'No '.$plural_name.' found in Trash',
		'parent_item_colon' => '',
		'menu_name' => $plural_name
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' =>  $slug ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','page-attributes')
	); 
	register_post_type( $post_name, $args);

	$labels = array(
	'name'              => 'Categories',
	'singular_name'     => 'Category',
	'search_items'      => 'Search Categories',
	'all_items'         => 'All Categories',
	'parent_item'       => 'Parent Category',
	'parent_item_colon' => 'Parent Category:',
	'edit_item'         => 'Edit Category',
	'update_item'       => 'Update Category',
	'add_new_item'      => 'Add New Category',
	'new_item_name'     => 'New Category Name',
	'menu_name'         => 'Categories',
	);

	$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => $post_name.'-categories' ),
	);

	register_taxonomy( $post_name.'-categories', $post_name, $args );
	register_taxonomy_for_object_type( $post_name.'-categories', $post_name ); 
	========================*/
}
/*======================
function bs_updated_messages( $messages ) {
	global $post, $post_ID;

	$singular_name = "Test";
	$plural_name = "Tests";
	global $post, $post_ID;
	$messages['test'] = array(
	0 => '', // Unused. Messages start at index 1.
	1 => sprintf( $singular_name.' updated. <a href="%s">View '.$singular_name.'</a>', esc_url( get_permalink($post_ID) ) ),
	2 => 'Custom field updated.',
	3 => 'Custom field deleted.',
	4 => $singular_name.' updated.',
	5 => isset($_GET['revision']) ? sprintf( $singular_name.' restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
	6 => sprintf( $singular_name.' published. <a href="%s">View '.$singular_name.'</a>', esc_url( get_permalink($post_ID) ) ),
	7 => $singular_name.' saved.',
	8 => sprintf( $singular_name.' submitted. <a target="_blank" href="%s">Preview '.$singular_name.'</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	9 => sprintf( $singular_name.' scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview '.$singular_name.'</a>',
	  // translators: Publish box date format, see http://php.net/date
	  date_i18n(  'M j, Y @ G:i', strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
	10 => sprintf( $singular_name.' draft updated. <a target="_blank" href="%s">Preview '.$singular_name.'</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'bs_updated_messages' );
========================*/
?>