<?php
function bigsplash_ajax_scripts() {
	wp_enqueue_script( 'bs_front_end_ajax_script', TEMPPATH . '/js/ajax.js', array( 'jquery' ), '1', false );
	wp_localize_script( 'bs_front_end_ajax_script', 'bs_ajax_script', array( 'bs_ajax' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce('valarmorghulis') ) );
}
add_action( 'wp_enqueue_scripts', 'bigsplash_ajax_scripts' );
add_action( 'wp_ajax_bs_ajax', 'bs_ajax_func' );
add_action( 'wp_ajax_nopriv_bs_ajax', 'bs_ajax_func' );
function bs_ajax_func() {
	check_ajax_referer( 'valarmorghulis', 'security' );
	$ajax_case = "";
	$data = array();
	if ( isset( $_POST['ajax_case'] ) ):
		$ajax_case = $_POST['ajax_case'];
	endif;
	
	switch ( $ajax_case ) {
		case "test":
			$test = $_POST['test'];
			$data['test'] = $test;
			echo json_encode( $data );
			break;
	}
	die();
}
?>